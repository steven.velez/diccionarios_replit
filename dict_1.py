# Ingresar el texto en una sola linea
# Contar el numero de veces q la palabra aparece antes
s = input("Ingrese las palabras seguidas de un espacio:\n")
contador = {}
listnum = []
resultado = ''
palabra = 0

for palabra in s.split():
    contador[palabra] = contador.get(palabra, 0) + 1
    k = contador[palabra] - 1
    listnum.append(k)
resultado = ' '.join(str(s) for s in listnum)

print("Numero de veces que se repiten las palabras\n",resultado)
