"""Dado un número n , seguido de n líneas de texto, imprima todas las palabras encontradas en el texto,
una por línea, con su número de ocurrencias en el texto. Las palabras deben clasificarse
en orden descendente según su número de ocurrencias,
y todas las palabras dentro de la misma frecuencia deben imprimirse en orden lexicográfico"""


n = int(input())
wordCollection = []
dictResult = {}
for i in range(n):
    s = input().split()
    for j in range(len(s)):
        wordCollection.append(s[j])
for i in range(len(wordCollection)):
    dictResult[wordCollection[i]] = wordCollection.count(wordCollection[i])
for name, occur in sorted(sorted(dictResult.items()), key=lambda key_value: key_value[1], reverse=True):
    print(name, occur)
