"""Dada una lista de países y ciudades de cada país, luego los
nombres de las ciudades. Para cada ciudad, imprima el país en el
 que se encuentra."""

a = int(input("¿Cuantas lineas necesita: ?\t"))
countries = {}
i = 0
print("Ingresa el pais y ciudades(seguidos de espacios)")
for i in range(a):
    s = [str(s) for s in input().split()]
    countries[s[0]] = [s[j] for j in range(1, len(s))]

m = int(input("¿Cuantas lineas necesitas ?\t"))
cities = []
j = 0
for j in range(m):
    city = input("Ciudad:\t")
    for country, city_list in countries.items():
        if city in countries[country]:
            print(country)
